# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.projectbase import ProjectBase


class PhaseIterator:
    """
    PhaseIterator is an iterator class for looping used in walk_phase_row.
    """
    def __init__(self, project, group, phase_row_count):
        self._project = project
        self._group = group
        self._phase_row_count = phase_row_count
        self._index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._index < self._phase_row_count:
            phase_row = self._project.get_phase_row_by_index(self._group, self._index)
            self._index += 1
        else:
            raise StopIteration()
        return phase_row


class PhaseGroup:
    """
    PhaseGroup is a container for multiple phase rows.
    """

    def __init__(self, project: ProjectBase, impl_data,  name: str, phase_group_count, phase_row_count: int):
        """
        Create a PhaseGroup object.

        :param project: The owning project object. It will be used for getting PhaseRow elements.
        :param impl_data: The underlying PhaseGroup implementation. It is needed by the owning project as reference.
        :param name: The user defined name of the PhaseGroup element.
        :param phase_group_count: The number of phases the PhaseGroup element has.
        :param phase_row_count: The number of PhaseRow element this PhaseGroup element has.
        """
        self._project = project
        self._impl_data = impl_data
        self._name = name
        self._phase_group_count = phase_group_count  # generally only one seen, is it possible to have more than one phase group?
        self._phase_row_count = phase_row_count

    @property
    def name(self):
        """
        Name of the PhaseGroup element.
        :type: str
        """
        return self._name

    @property
    def phase_group_count(self):
        """
        Number of the PhaseGroup elements.

        :type: int
        """
        return self._phase_group_count

    @property
    def phase_row_count(self):
        """
        Number of the PhaseRow elements.

        :type: int
        """
        return self._phase_row_count

    def get_phase_row_by_index(self, phase_row_index: int):
        """
        Returns a PhaseRow element.

        :param phase_row_index: PhaseRow index of the PhaseRow element.
        :return: PhaseRow element
        :type: PhaseRow
        """
        if not phase_row_index < self.phase_row_count:
            raise IndexError("PhaseGroup has {} phase element, but {} was requested".format(self._phase_row_count,
                                                                                            phase_row_index))
        return self._project.get_phase_row_by_index(self._impl_data, phase_row_index)

    def walk_phase_row(self, phase_row_index=0):
        """
        return an iterator, which enables the user to iterate over PhaseRow elements.

        Example::
            for ds in project.walk_phase_row():
                print([ds.name, ds.id])

        :param phase_row_index: index of the phase tracks that are returned
        :return:
        """
        if not phase_row_index < self._phase_row_count:
            raise IndexError("requested phase-row index: {}, max phase-row index: {}".format(phase_row_index,
                                                                                             self._phase_row_count - 1))

        return PhaseIterator(self._project, self._impl_data, self._phase_row_count)
