# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.projectbase import ProjectBase


class DataRow:

    def __init__(self, project: ProjectBase, impl_parent, impl_data, name: str, type_name: str, short_type_name: str,
                 unit_name: str, _id: int, size: int, first: float, last: float):
        """
        Create a DataRow element object.

        This should never be called directly! Use a DataGroup element for obtaining a DataRow element.

        :type project: The owning project object. It will be used for getting DataRow elements.
        :type impl_parent: The underlying DataGroup implementations, which is the parent of this DataRow element.
        :type impl_data: The underlying DataRow implementation. It is needed by the owning project as reference.
        :type name: User specified name of this DataRow element.
        """
        self._project = project
        self._impl_parent = impl_parent
        self._impl_data = impl_data
        self._name = name
        self._type_name = type_name
        self._short_type_name = short_type_name
        self._unit_name = unit_name
        self._id = _id
        self._size = size
        self._first = first
        self._last = last

    def get_data_row(self):
        """
        Returns the data row.
        :type: numpy.array
        """
        return self._project.get_data_row(self._impl_data)

    def get_all_data_rows(self):
        """
        Returns all tracks for this DataRow element combined.
        :return:
        :type: numpy.array
        """
        return self._project.get_all_data_rows(self._impl_parent, self._id)

    @property
    def name(self):
        """
        Name of the DataRow element
        :type: str 
        """
        return self._name

    @property
    def type_name(self):
        """

        :type: str,
        """
        return self._type_name

    @property
    def short_type_name(self):
        """

        :type: str
        """
        return self._short_type_name

    @property
    def unit_name(self):
        """
        Name of the units measured
        :type: str
        """
        return self._unit_name

    @property
    def id(self):
        """
        ID of the DataRow element
        :type: int
        """
        return self._id

    @property
    def size(self):
        """
        Number of data points in DataRow track
        :type: int
        """
        return self._size

    @property
    def frequency(self):
        """
        Collection frequency of the data (Hz)
        :type: float
        """
        return (self._size - 1) / (self._last - self._first)

    @property
    def first(self):
        """

        :type: float
        """
        return self._first

    @property
    def last(self):
        """

        :type: float
        """
        return self._last

    @property
    def minimum(self):
        """
        Minimum value of the data Track
        :type: float
        """
        return min(self.get_data_row())

    @property
    def maximum(self):
        """
        Maximum value of the data Track
        :type: float
        """
        return max(self.get_data_row())
