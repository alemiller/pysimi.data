# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.data_group import DataGroup
from pysimi.data.classes.magickeys import MagicKeys
from pysimi.data.classes.projectbase import ProjectBase

from mockito import when

import unittest


class TestDataGroup(unittest.TestCase):

    def test_initialization_properties(self):
        data_group = DataGroup(None, None, 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)

        self.assertEqual(42, data_group.id)
        self.assertEqual(MagicKeys.DATA_GROUP_RAW, data_group.magick)
        self.assertEqual("Forty Two", data_group.name)
        self.assertEqual(2, data_group.track_count)
        self.assertEqual(21, data_group.data_row_count)

    def test_get_data_row_by_index_calls_project(self):
        project = ProjectBase(path=None)
        when(project).get_data_row_by_index("group", 1, 2).thenReturn("found group 2")
        data_group = DataGroup(project, "group", 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)
        self.assertEqual("found group 2", data_group.get_data_row_by_index(1, 2))

    def test_get_data_row_by_index_raises_exceptions(self):
        data_group = DataGroup(None, None, 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)

        with self.assertRaises(IndexError):
            data_group.get_data_row_by_index(0, 22)  # data index is to big

        with self.assertRaises(IndexError):
            data_group.get_data_row_by_index(5, 0)  # track index is too big

    def test_get_data_row_by_id_calls_project(self):
        project = ProjectBase(path=None)
        when(project).get_data_row_by_id("group1", 0, 123456).thenReturn("found group 123456")
        data_group = DataGroup(project, "group1", 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)
        self.assertEqual("found group 123456", data_group.get_data_row_by_id(0, 123456))

    def test_get_data_row_by_id_raises_exception(self):
        data_group = DataGroup(None, None, 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)

        with self.assertRaises(IndexError):
            data_group.get_data_row_by_id(5, 0)  # track number is too big

    def test_walk_data_row(self):
        project = ProjectBase(path=None)
        when(project).get_data_row_by_index(...).thenReturn(1)
        impl_data = None
        data_group = DataGroup(project, impl_data, 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)

        counter = 0

        for data_row in data_group.walk_data_row(0):
            counter += data_row

        self.assertEqual(21, counter)

    def test_walk_data_row_raises_exception(self):
        data_group = DataGroup(None, None, 42, MagicKeys.DATA_GROUP_RAW, "Forty Two", MagicKeys.DATA_GROUP_RAW.tracks, 21)

        with self.assertRaises(IndexError):
            data_group.walk_data_row(MagicKeys.DATA_GROUP_RAW.tracks)


if __name__ == '__main__':
    unittest.main()
