# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.data_row import DataRow
from pysimi.data.classes.projectbase import ProjectBase

from mockito import when

import unittest


class TestDataRow(unittest.TestCase):

    def test_initialization_properties(self):
        data_row = DataRow(None, None, None, "name", "long type name", "short typo name", "unit name", 1234, 17, 0.2, 0.3)

        self.assertEqual("name", data_row.name)
        self.assertEqual("long type name", data_row.type_name)
        self.assertEqual("short typo name", data_row.short_type_name)
        self.assertEqual("unit name", data_row.unit_name)
        self.assertEqual(1234, data_row.id)
        self.assertEqual(17, data_row.size)
        self.assertEqual(0.2, data_row.first)
        self.assertEqual(0.3, data_row.last)

    def test_get_data_row_calls_project(self):
        project = ProjectBase(path=None)
        when(project).get_data_row("row1").thenReturn("1D data row")

        data_row = DataRow(project, None, "row1", "name", "long type name", "short typo name", "unit name", 1234, 17, 0.2, 0.3)

        self.assertEqual("1D data row", data_row.get_data_row())

    def test_get_all_data_rows_calls_project(self):
        project = ProjectBase(path=None)
        when(project).get_all_data_rows("parent", 1234).thenReturn("3D data row")

        data_row = DataRow(project, "parent", None, "name", "long type name", "short typo name", "unit name", 1234, 17, 0.2, 0.3)

        self.assertEqual("3D data row", data_row.get_all_data_rows())


if __name__ == '__main__':
    unittest.main()
