# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.phase_row import PhaseRow
import unittest


class TestDataRow(unittest.TestCase):

    def test_initialization_properties(self):
        data_row = PhaseRow(None, None, None, "name", "start time", "end time")

        self.assertEqual("name", data_row.name)
        self.assertEqual("start time", data_row.start)
        self.assertEqual("end time", data_row.end)


if __name__ == '__main__':
    unittest.main()
